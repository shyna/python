# Archivo con la clase para el manejo de la API
import requests # Libreria externa, instalar
import json

class apiManagement:

    # Constructor de la clase
    def __init__(self, apiKey):
        self.apiKey=apiKey

    def convertFtoC(self, temp): 
        celsius = (temp - 32) * 5/9
        return celsius

    # Función para consultar la API
    def consultarApi(self, fecha, location):
        # Ejemplo de la API
        # https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/[location]/[date1]/[date2]?key=YOUR_API_KEY 
        self.query = requests.get("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/"+str(fecha)+"/"+str(location)+"/?key="+str(self.apiKey)+"&include=current&lang=es")
        array=json.loads(self.query.text)
        address=array["resolvedAddress"]
        temp_max=round(self.convertFtoC(float(array["days"][0]["tempmax"])),2)
        temp_min=round(self.convertFtoC(float(array["days"][0]["tempmin"])),2)
        temp_middle=round(self.convertFtoC(float(array["days"][0]["temp"])),2)
        return [address,temp_max,temp_min,temp_middle]
