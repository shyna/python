from datetime import datetime
# Archivo externo con datos privados(apiKey y location)
from private_data import *
# Archivo con la clase para el manejo de la API
from api import apiManagement
# Archivo con la clase para el entorno gráfico
from visual import visual

# Fecha a consultar
fecha=str(datetime.strftime(datetime.now(), "%Y-%m-%dT%H:%M:%S"))
# Creación de conexión con la API
conexionApi = apiManagement(apiKey)

# Datasheet(address, temp_max, temp_min, temp_middle)
datasheet=conexionApi.consultarApi(location,fecha)

# Imprimir
see=visual(datasheet)
